<?php

header("Access-Control-Allow-Origin: *");

$type = $_POST["type"];

if($type == "getAuth") {
   $headers = array(
       'Authorization: Basic '. base64_encode("api-key"),
       'Content-Type: application/json',
       'Accept: application/json'
   );
   $curl = curl_init();
   curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
   curl_setopt_array($curl, array(
       CURLOPT_RETURNTRANSFER => 1,
       CURLOPT_URL => 'https://api.afterpay.com/v1/configuration'
   ));
   $resp = curl_exec($curl);
   curl_close($curl);
   echo $resp;
} elseif($type == "createOrder") {
  $headers = array(
      'Authorization: Basic '. base64_encode("api-key"),
      'Content-Type: application/json',
      'Accept: application/json'
  );
  $total = $_POST["total"];
  $first_name = $_POST["first_name"];
  $last_name = $_POST["last_name"];
  $phone = $_POST["phone"];
  $email = $_POST["email"];

  $data = array(
    "totalAmount" => array("amount" => $total, "currency" => "AUD"),
    "consumer" => array(
      "phoneNumber" => $phone,
      "givenNames" => $first_name,
      "surname" => $last_name,
      "email" => $email
    ),
    "merchant" => array(
      "redirectConfirmUrl" => "https://design4.mitchellcreative.com.au/success",
      "redirectCancelUrl" => "https://design4.mitchellcreative.com.au/checkout"
    )
  );
  $jsonData = json_encode($data);
  $curl = curl_init('https://api.afterpay.com/v1/orders');
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $resp = curl_exec($curl);
  curl_close($curl);
  echo($resp);

} elseif($type == "payment") {
  $ref = $_POST["reference"];
  $token = $_POST["token"];
  $headers = array(
      'Authorization: Basic '. base64_encode("api-key"),
      'Content-Type: application/json',
      'Accept: application/json' // <---
  );

  $data = array(
    "token" => $token,
    "merchantReference" => $ref
  );
  $jsonData = json_encode($data);
  // Get cURL resource
  $curl = curl_init('https://api.afterpay.com/v1/payments/capture');

  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

  $resp = curl_exec($curl);
  curl_close($curl);

  echo($resp);
} else {
  echo "Error";
}





 ?>
