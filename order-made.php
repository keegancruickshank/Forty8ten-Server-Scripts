<?php
header("Access-Control-Allow-Origin: *");
require 'vendor/autoload.php';

use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

$firstName = $_POST["first_name"];
$lastName = $_POST["last_name"];
$phone = $_POST["phone"];
$address = $_POST["address"];
$email = $_POST["email"];
$base = $_POST["base"];
$shape = $_POST["shape"];
$construction = $_POST["construction"];
$topsheet = $_POST["topsheet"];
$base = $_POST["base"];
$sidewall = $_POST["sidewall"];
$price = $_POST["price"];


$httpClient = new GuzzleAdapter(new Client());
$sparky = new SparkPost($httpClient, ['key'=>'spark-key']);

$promise = $sparky->transmissions->post([
    'content' => ['template_id' => 'new-order'],
    'substitution_data' => ['first_name' => $firstName, 'last_name' => $lastName, 'phone' => $phone, 'address_postal' => $address, 'email' => $email, 'base' => $base, 'shape' => $shape, 'construction' => $construction, 'topsheet' => $topsheet, 'base' => $base, 'sidewall' => $sidewall, 'price' => $price],
    'recipients' => [
        [
            'address' => [
                'name' => 'Keegan Cruickshank',
                'email' => 'keegan@mitchellcreative.com.au',
            ],
        ],
    ],
]);

try {
    $response = $promise->wait();
    echo $response->getStatusCode()."\n";
    print_r($response->getBody())."\n";
} catch (\Exception $e) {
    echo $e->getCode()."\n";
    echo $e->getMessage()."\n";
}
?>
